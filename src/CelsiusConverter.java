public class CelsiusConverter implements TempConverter {

    @Override
    public double toFahrenheit(double source) {
        return ((source * 1.8) + 32);
    }

    @Override
    public double toKelvin(double source) {
        return (source + 273.15);
    }
}
