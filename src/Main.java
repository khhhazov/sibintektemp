import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Введите тепературу в градусах Цельсия:");
        Scanner input = new Scanner(System.in);
        double source = input.nextDouble();

        TempConverter converter = new CelsiusConverter();
        System.out.printf("Градусы Фаренгейта: %f\n", converter.toFahrenheit(source));
        System.out.printf("Градусы Кельвина: %f\n", converter.toKelvin(source));
    }
}
