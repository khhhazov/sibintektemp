public interface TempConverter {
    double toFahrenheit(double source);
    double toKelvin(double source);
}
